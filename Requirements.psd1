@{
    PSDependOptions  = @{
        source = 'PSGallery'
    }
    Plaster          = 'latest'
    InvokeBuild      = 'latest'
    Pester           = 'latest'
    PSScriptAnalyzer = 'latest'
    BuildHelpers     = 'latest'
}

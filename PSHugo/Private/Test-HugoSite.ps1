
Function Test-HugoSite {
    <#
    .SYNOPSIS
        Test for a hugo site in the given directory
    .DESCRIPTION
        To validate that the directory is a hugo site, validate that:
        - there is a 'content' directory
        - there is a 'layouts' directory
        - there is a 'config.[toml|json|yaml]' file
        - the config file has a line that contains 'baseURL'
    #>
    [CmdletBinding()]
    param(    # Optionally give another directory to start in
        [Parameter(
            Mandatory = $false,
            ValueFromPipeline = $true
        )]
        [ValidateScript(
            {
                if (-Not ($_ | Test-Path)) {
                    throw "$_ does not exist"
                }
                return $true
            }
        )]
        [System.IO.FileInfo]
        $Path = (Get-Location).ToString()
    )

    $directory_list = @('content', 'layouts')
    Write-Verbose "Looking in $($Path.FullName) for hugo site information"
    foreach ($dir in $directory_list) {
        if (Test-Path "$Path\$dir") {
            Write-Verbose "$dir found"
        } else {
            Write-Verbose "$dir not found"
            return $false
        }
    }
    $config_files = Get-ChildItem $Path -Include ("config.toml","config.yaml", "config.json") -File -Depth 0

    if ($config_files.Count -gt 0) {
        if ( Select-String $config_files[0] -Pattern "baseURL" ) {
            Write-Verbose "BaseURL found in $($config_files[0].BaseName)"
        } else {
            Write-Verbose "baseURL not found"
            return $false
        }
    } else {
        Write-Verbose "config file not found"
        return $false
    }

    return $true
}

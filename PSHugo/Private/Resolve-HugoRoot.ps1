
Function Resolve-HugoRoot {
    <#
    .SYNOPSIS
        Recursively search up the directory to find the root
        of the site
    #>
    [CmdletBinding()]
    param(
        # Optionally give another directory to start in
        [Parameter(
            Mandatory = $false,
            ValueFromPipeline = $true
        )]
        [ValidateScript(
            {
                if (-Not ($_ | Test-Path)) {
                    throw "$_ does not exist"
                }
                return $true
            }
        )]
        [System.IO.FileInfo]
        $Path = (Get-Location).ToString(),

        # Optionally limit how many levels to check
        [Parameter(
            Mandatory = $false,
            ValueFromPipeline = $true
        )]
        [int]
        $Depth = 4
    )

    $site_found = $false
    $current_dir = (Get-Item $Path)
    Write-Verbose "Starting in $current_dir"
    Write-Verbose "Looking for site root with a maximum depth of $Depth"
    for ($i = 0; $i -lt $Depth; $i++) {
        if (Test-HugoSite $current_dir.ToString()) {
            Write-Verbose "hugo site found at level $i"
            $site_found = $true
            break
        } else {
            $current_dir = $current_dir.Parent
            Write-Verbose "Looking in $($current_dir.Name)"
        }
    }

    if ( -Not $site_found) {
        throw "Site root not found in $Depth levels"
    }

    $current_dir
}

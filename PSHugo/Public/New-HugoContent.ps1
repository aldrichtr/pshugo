
Function New-HugoContent {
    <#
    .SYNOPSIS
        Create new content using 'hugo new'
    #>
    [CmdletBinding()]
    param(
        # The type of content
        [Parameter(
            Mandatory = $true,
            ValueFromPipeline = $true
        )]
        [string]
        $Type,

        # The title
        [Parameter(
            Mandatory = $true,
            ValueFromPipeline = $true
        )]
        [string]
        $Title,

        # The format of the content
        [Parameter(
            Mandatory = $false,
            ValueFromPipeline = $true
        )]
        [ValidateSet('markdown',
            'org-mode',
            'restructured',
            'pandoc',
            'asciidoc',
            'html'
        )]
        [string]
        $Format = 'markdown'
    )

    $extention_map = @{
        'markdown' = 'md'
        'org'      = 'org'
        'html'     = 'html'
        'asciidoc' = 'ad'
        'pandoc'   = 'pdc'
    }

    [string[]]$action = @()
    $file_name = ($Title -replace ' ', '-').ToLower()
    $new_content = "$Type/$file_name.$($extention_map[$Format])"

    Write-Verbose "calling hugo to make $new_content"

    $action = "${HugoExecutable} new $new_content"

    $action += '2>&1'
    [scriptblock]$action = [scriptblock]::Create($action)

    $all_output = & $action

    $execErrors = $all_output | Where-Object { $_ -is [System.Management.Automation.ErrorRecord]}
    $result     = $all_output | Where-Object { $_ -isnot [System.Management.Automation.ErrorRecord]}

    if ( $execErrors ) {
        throw $execErrors
    }

    Write-Debug $result

}

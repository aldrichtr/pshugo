
Function Get-HugoContent {
    <#
    .SYNOPSIS
        Get a list of hugo content files.
    .DESCRIPTION
        Get hugo content files from the current hugo site.  First, the function
        will look for the root of the site (either at the current directory, or
        starting at the provided 'Path').  Next, the function will gather all
        the files in the content directory based on:
        - the Format (markdown by default)
        - the Type (all content by default, or Type can be one or more hugo
        content types)
        - Index pages (an exclusion filter.  If set will only return index
        pages)
    .EXAMPLE
        PS C:\hugo-website> $pages = Get-HugoContent
    .EXAMPLE
        PS C:\hugo-website> $indexes = Get-HugoContent -Index
    .EXAMPLE
        PS C:\hugo-website> $about_index = Get-HugoContent -Type 'about' -Index
    #>
    [CmdletBinding()]
    param(
        # Optionally give a path (current directory by default)
        [Parameter(
            Mandatory = $false,
            ValueFromPipeline = $true
        )]
        [Alias('PSPath')]
        [string]
        $Path = (Get-Location).ToString(),

        # Format of content files
        [Parameter(
            Mandatory = $false,
            ValueFromPipeline = $true
        )]
        [ValidateSet('markdown',
            'org-mode',
            'restructured',
            'pandoc',
            'asciidoc',
            'html'
        )]
        [string]
        $Format = 'markdown',

        # The content type
        [Parameter(
            Mandatory = $false,
            ValueFromPipeline = $true
        )]
        [string[]]
        $Type,

        # If set, will only return the index files
        # combine with type to get the index file for a specific type
        [Parameter(
            Mandatory = $false,
            ValueFromPipeline = $true
        )]
        [switch]
        $Index
    )

    $content = @()
    $extention_map = @{
        'markdown' = @('*.md')
        'org'      = @('*.org')
        'html'     = @('*.htm', '*.html')
        'asciidoc' = @('*.ad', '*.adoc')
        'pandoc'   = @('*.pdc', '*.pandoc')
    }

    try {
        $site_root = Resolve-HugoRoot $Path
    } catch {
        throw "$Path is not a hugo project"
    }


    if ($PSBoundParameters['Type']) {
        foreach ($t in $Type) {
            $content += Get-ChildItem -Path "$site_root\content\$Type" -Include $extention_map[$Format] -Recurse
        }
    } else {
        $content += Get-ChildItem -Path "$site_root\content" -Include $extention_map[$Format] -Recurse
    }

    if ($Index) {
        $content = $content | Where-Object { $_.BaseName -like '_index' }
    }

    $content
}

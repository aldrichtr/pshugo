$HugoExecutable = 'hugo'
Function Invoke-HugoServer {
    [CmdletBinding()]
    param(
      # Display draft pages
      [Parameter(
          Mandatory = $false,
          ValueFromPipeline = $false
      )]
      [switch]
      $buildDrafts
    )

    $action = "${HugoExecutable}"

    $action += 'server'

    if ($buildDrafts) {
        $action += '--buildDrafts'
    }

    $action += '2>&1'

    [scriptblock]$action = [scriptblock]::Create($action)

    $allOutput = & $action


    $execErrors = $allOutput | Where-Object { $_ -is [System.Management.Automation.ErrorRecord]}
    $result     = $allOutput | Where-Object { $_ -isnot [System.Management.Automation.ErrorRecord]}

    if ( $execErrors ) {
        throw $execErrors
    }

    $allOutput
}
